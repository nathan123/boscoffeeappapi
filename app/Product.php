<?php

namespace BosCoffee;


use Illuminate\Database\Eloquent\Model;

class Product extends Model {

    protected $table = 'products';

}