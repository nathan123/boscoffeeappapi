<html>
	<head>
		<title>Laravel</title>
		
		<link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

	</head>
	<body>
		<div class="container">
			<div class="content">
                <img src="data:image/png;base64, {{ base64_encode( $code ) }} ">
			</div>
		</div>
	</body>
</html>
