<?php

namespace BosCoffee\Http\Controllers;


use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRcodeController extends Controller {

    public function generate() {

        QrCode::format('png')->size(100);

        return view('welcome')->with('code', QrCode::generate('BOSCOFEE'));
    }
}