<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/**
 * Endpoint Services
 */
Route::group(['prefix' => 'v1', 'before' => 'oauth' ], function() {

    Route::get('product', 'ProductController@getAllProducts');
    Route::get('product/{productId}', 'ProductController@getSingleProduct');
    Route::get('qrcode', 'QRcodeController@generate');

});

/**
 * User Authentication
 */
Route::post('v1/user/login', function() {

    if( Auth::check() ) {

        $data = array(
            'user' => Auth::user(),
            'auth' => Authorizer::issueAccessToken()
        );

        return Response::json( $data );
    }

    return Response::json( array(
        'Login Failed'
    ), 401 );

});

/**
 * User Authentication Destroy
 */
Route::post('v1/user/logout', function() {

    if(Auth::check()) {

        $rule = array(
            'access_token' => 'required|exists:oauth_access_tokens,id'
        );

        $validator = Validator::make(Input::all(), $rule);

        if( $validator->fails() )
        {
            return response()->json( $validator->messages() );
        }

        // get the access token
        $access_token = Request::input('access_token');

        // get the session id by access token
        $session_id = \BosCoffee\OauthAccessToken::getSessionIdByToken( $access_token );

        // remove the user session
        \BosCoffee\OauthSession::remove( $session_id );

        // remove the access token
        \BosCoffee\OauthAccessToken::remove( $access_token );


        return response()->json(array(
            'message' => 'Successfully Logout'
        ));
    }

    return response()->json( array(
        'message' => 'You are not currently logged in'
    ), 404 );

});
