<?php
/**
 * Created by PhpStorm.
 * User: macbookair
 * Date: 6/6/15
 * Time: 3:26 PM
 */

namespace BosCoffee;


use Illuminate\Database\Eloquent\Model;

class OauthSession extends Model {

    protected $table = 'oauth_sessions';

    public static function remove( $sessionId )
    {
        if( OauthSession::where( 'id', $sessionId )->delete() )
        {
            return true;
        }
        return false;
    }
}