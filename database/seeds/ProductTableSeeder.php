<?php

class ProductTableSeeder extends \Illuminate\Database\Seeder {

    public function run()
    {
        $products = config('services.boscoffee.products');

        foreach( $products as $type => $product )
        {
            foreach( $product as $data )
            {
                \BosCoffee\Product::create(array(
                    'name' => $data->name,
                    'type' => $type,
                    'description' => $data->description,
                    'price' => 12.00,
                    'quantity' => 20,
                    'image' => $data->image
                ));
            }
        }
    }
}