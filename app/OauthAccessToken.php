<?php
/**
 * Created by PhpStorm.
 * User: macbookair
 * Date: 6/6/15
 * Time: 3:24 PM
 */

namespace BosCoffee;

use Illuminate\Database\Eloquent\Model;

class OauthAccessToken extends Model {

    protected $table = 'oauth_access_tokens';

    public static function remove( $access_token )
    {
        if( OauthAccessToken::where( 'id', $access_token )->delete() ) {
            return true;
        }
        return false;
    }

    public static function getSessionIdByToken( $access_token )
    {
        return OauthAccessToken::where( 'id', $access_token )->pluck('session_id');
    }
}