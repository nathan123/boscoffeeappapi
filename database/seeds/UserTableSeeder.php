<?php

use Illuminate\Database\Seeder;
use BosCoffee\User;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder {

    public function run()
    {
        User::create(
            array(
                'name' => 'King James',
                'email' => 'tcorp.kingjames@gmail.com',
                'username' => 'kingjames',
                'password' => Hash::make('kingjames')

            )
        );
    }
}