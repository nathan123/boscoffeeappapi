<?php namespace BosCoffee\Http\Controllers;

use BosCoffee\Http\Requests;
use BosCoffee\Product;

class ProductController extends Controller {

	/**
	 * Get all products.
	 *
	 * @return Response
	 */
	public function getAllProducts() {

        $products = Product::all();
		return response()->json( $products, 200 );
	}

    /**
     * Get single product
     * @param $productId
     * @return \Illuminate\Support\Collection|null|static
     */
    public function getSingleProduct( $productId ) {

        return Product::find( $productId );
    }
}
